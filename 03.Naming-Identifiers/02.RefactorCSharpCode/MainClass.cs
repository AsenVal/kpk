﻿using System;

public class MainClass
{
    public enum Sex
    {
        Male, Fimale
    };

  public class Human
  {
    public Sex Sex { get; set; }
    public string Name { get; set; }
    public int Age { get; set; }
  }

  public void CreateHuman(int humanNumber)
  {
      Human human = new Human();
      human.Age = humanNumber;
      if (humanNumber % 2 == 0)
      {
          human.Name = "Батката";
          human.Sex = Sex.Male;
      }
      else
      {
          human.Name = "Мацето";
          human.Sex = Sex.Fimale;
      }
  }
}

