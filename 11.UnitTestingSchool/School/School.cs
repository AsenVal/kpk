﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolProject
{
    public class School
    {
        private string name;
        private IList<Course> courses;
        public const int MinStudentUniqueNumber = 10000;
        public const int MaxStudentUniqueNumber = 99999;
        private int currentStudentUniqueNumber = MinStudentUniqueNumber;
        
        /// <summary>
        /// Gets or sets the name of the school.
        /// </summary>
        /// <value>Gets or sets the value of the name field.</value>
        /// <exception cref="System.ArgumentException">Thrown when
        /// the value to set is null, empty or contains only
        /// whitespace characters.</exception>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("Course name cannot be null or empty.", "value");
                }

                this.name = value;
            }
        }

        /// <summary>
        /// Gets the list of courses taught at the school.
        /// </summary>
        /// <value>Gets the value of the courses list.</value>
        public IList<Course> Courses
        {
            get
            {
                return new ReadOnlyCollection<Course>(this.courses);
            }
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="School"/> class.
        /// </summary>
        /// <param name="name">School name.</param>
        public School(string name)
        {
            this.Name = name;
            this.courses = new List<Course>();
        }

        /// <summary>
        /// Adds a course to the list of courses taught at the school.
        /// </summary>
        /// <param name="course">The course to add.</param>
        /// <exception cref="System.ArgumentNullException">Thrown when
        /// <paramref name="course"/> is null.</exception>
        public void AddCourse(Course course)
        {
            if (course == null)
            {
                throw new ArgumentNullException("course", "course cannot be null.");
            }

            this.courses.Add(course);
        }

        /// <summary>
        /// Removes a course passed as a <see cref="Course"/> instance.
        /// </summary>
        /// <param name="course">The course to remove.</param>
        /// <returns>True if operation succeeds, otherwise - false.</returns>
        public bool RemoveCourse(Course course)
        {
            return this.courses.Remove(course);
        }

        /// <summary>
        /// Changes current student unique number.
        /// </summary>
        /// <returns>Returns last available student unique number.</returns>
        internal int GetUniqueNumber()
        {
            currentStudentUniqueNumber++;
            return currentStudentUniqueNumber;
        }
    }
}
