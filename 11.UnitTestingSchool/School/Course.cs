﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace SchoolProject
{
    public class Course
    {
        private string name;
        private IList<Student> students;
        public const int MaxStudentsCount = 30;

        /// <summary>
        /// Gets or sets the name of the course.
        /// </summary>
        /// <value>Gets or sets the value of the name field.</value>
        /// <exception cref="System.ArgumentException">Thrown when
        /// the value to set is null, empty or contains only
        /// whitespace characters.</exception>
        public string Name
        {
            get { return name; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    {
                        throw new ArgumentException("You must enter at least one charackter for student name.");
                    }
                }
                else
                {

                    name = value;
                }
            }
        }
        /// <summary>
        /// Gets the list of students attending the course.
        /// </summary>
        /// <value>Gets the value of the students list.</value>
        public IList<Student> Students
        {
            get { return new ReadOnlyCollection<Student>(this.students); }

        }

        /// <summary>
        /// Adds a student to the list of students attending the course.
        /// </summary>
        /// <param name="student">The student joining the course.</param>
        /// <exception cref="System.ArgumentNullException">Thrown when
        /// <paramref name="student"/> is null.</exception>
        public void Join(Student student)
        {
            if (student == null)
            {
                throw new ArgumentNullException("student", "Student cannot be null.");
            }
            if (students.Count == MaxStudentsCount)
            {
                throw new InvalidOperationException("Student cannot be added. Course attendants have reached the maximum number " + MaxStudentsCount);
            }
            this.students.Add(student);
        }

        /// <summary>
        /// Removes a student passed as a <see cref="Student"/> instance.
        /// </summary>
        /// <param name="student">The student to leave the course.</param>
        /// <returns>True if operation succeeds, otherwise - false.</returns>
        public bool Leave(Student student)
        {
            return this.students.Remove(student);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Course"/> class.
        /// </summary>
        /// <param name="name">The name of the course.</param>
        public Course(string name)
        {
            this.Name = name;
            this.students = new List<Student>();
        }

        /// <summary>
        /// Returns the string representation of the course info - name, students.
        /// </summary>
        /// <returns>A string which represents the course info.</returns>
        public override string ToString()
        {
            string result = string.Format("Name = {0}; Students = {1}.", this.Name, this.GetStudentsAsString());
            return result;
        }

        /// <summary>
        /// Converts the list of students into a string.
        /// </summary>
        /// <returns>A string which contains all the names in the students list,
        /// comma-separated and surrounded with curly brackets.</returns>
        private string GetStudentsAsString()
        {
            if (this.students.Count == 0)
            {
                return "{ }";
            }
            else
            {
                return "{ " + string.Join(", ", this.students) + " }";
            }
        }
    }
}
