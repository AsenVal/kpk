﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SchoolProject;

namespace UnitTestSchool
{
    [TestClass]
    public class StudentTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void StudentConstructorNullInput()
        {
            School school = new School("Telerik");
            Student student = new Student(null, school);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void StudentConstructorWithspaceInput()
        {
            School school = new School("Telerik");
            Student student = new Student("  ", school);
        }

        [TestMethod]
        public void StudentConstructorStudentName()
        {
            School school = new School("Telerik");
            Student student = new Student("Ivan Petrov", school);
            Assert.AreEqual("Ivan Petrov", student.Name, "Student's name is not set correctly");
        }

        [TestMethod]
        public void StudentConstructorStudentNumberNext()
        {
            School school = new School("Telerik");
            Student IvanPetrov = new Student("Ivan Petrov", school);
            Student StoyanIvanov = new Student("Stoyan Ivanov", school);
            Assert.IsTrue(IvanPetrov.UniqueNumber == StoyanIvanov.UniqueNumber -1, "Student's unique number is not set sequently");
        }

        [TestMethod]
        public void StudentStudentToString()
        {
            School school = new School("Telerik");
            Student IvanPetrov = new Student("Ivan Petrov", school);
            Assert.AreEqual(IvanPetrov.ToString(), string.Format("{0} with unique number {1}.", IvanPetrov.Name, IvanPetrov.UniqueNumber), "Student.ToString() is not correct");
        }
    }
}
