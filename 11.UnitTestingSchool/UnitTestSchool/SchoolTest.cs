﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SchoolProject;

namespace UnitTestSchool
{
    [TestClass]
    public class SchoolTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SchoolConstructorNullInput()
        {
            School Telerik = new School(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SchoolConstructorWhitespaceInput()
        {
            School Telerik = new School("   ");
        }

        [TestMethod]
        public void SchoolConstructorName()
        {
            School Telerik = new School("Telerik");
            Assert.AreEqual(Telerik.Name, "Telerik", "School's name is not set correctly");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SchoolAddNullCourseInput()
        {
            School Telerik = new School("Telerik");
            Telerik.AddCourse(null);
        }

        [TestMethod]
        public void SchoolAddCourseInstance()
        {
            Course KPK = new Course("KPK");
            School Telerik = new School("Telerik");
            Telerik.AddCourse(KPK);
            Assert.AreEqual(Telerik.Courses[0], KPK, "TheCourse is not added correctly");
        }

        [TestMethod]
        public void SchoolAddCourse()
        {
            Course KPK = new Course("KPK");
            School Telerik = new School("Telerik");
            int currentCoursesCount = Telerik.Courses.Count;
            Telerik.AddCourse(KPK);
            Assert.AreEqual(currentCoursesCount + 1, Telerik.Courses.Count, "Could not add course to the school");
        }

        [TestMethod]
        public void SchoolRemoveExistingCourse()
        {
            School Telerik = new School("Telerik");
            Course KPK = new Course("KPK");
            Course OOP = new Course("OOP");
            Telerik.AddCourse(KPK);
            Telerik.AddCourse(OOP);
            int currentCoursesCount = Telerik.Courses.Count;
            bool isRemoved = Telerik.RemoveCourse(KPK);
            Assert.IsTrue(isRemoved, "Course is not existing to the student");
            Assert.AreEqual(currentCoursesCount - 1, Telerik.Courses.Count, "Course is not removed from course list");
        }

        [TestMethod]
        public void SchoolRemoveNonExistingCourse()
        {
            School Telerik = new School("Telerik");
            Course KPK = new Course("KPK");
            Course OOP = new Course("OOP");
            Course JS1 = new Course("JS1");
            Telerik.AddCourse(KPK);
            Telerik.AddCourse(OOP);
            int currentCoursesCount = Telerik.Courses.Count;
            bool isRemoved = Telerik.RemoveCourse(JS1);
            Assert.IsFalse(isRemoved, "Non-existent course removed.");
            Assert.AreEqual(currentCoursesCount, Telerik.Courses.Count, "Non-existent course removed.");
        }

        [TestMethod]
        public void StudentUniqueNumberIsBiggerThanMin()
        {
            School Telerik = new School("Telerik");
            Student Ivan = new Student("Ivan Petrov", Telerik);
            Assert.IsTrue(Ivan.UniqueNumber > School.MinStudentUniqueNumber, "Student number is not propriate.");
        }

        [TestMethod]
        public void StudentUniqueNumberIsSmallerThanMax()
        {
            School Telerik = new School("Telerik");
            Student Ivan = new Student("Ivan Petrov", Telerik);
            Assert.IsTrue(Ivan.UniqueNumber < School.MaxStudentUniqueNumber, "Student number is not propriate.");
        }
    }
}
