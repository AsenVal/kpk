﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SchoolProject;

namespace UnitTestSchool
{
    /// <summary>
    /// Summary description for CourseTest
    /// </summary>
    [TestClass]
    public class CourseTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CourseConstructorNullInput()
        {
            Course KPK = new Course(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CourseConstructorWhitespaceInput()
        {
            Course KPK = new Course("   ");
        }

        [TestMethod]
        public void CourseConstructorName()
        {
            Course KPK = new Course("KPK");
            Assert.AreEqual(KPK.Name, "KPK", "Course's name is not set correctly");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CourseJoinNullStudentInput()
        {
            Course KPK = new Course("KPK");
            KPK.Join(null);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CourseJoinMoreThanMaxStudentInput()
        {
            Course KPK = new Course("KPK");
            School Telerik = new School("Telerik");
            for (int i = 0; i < Course.MaxStudentsCount + 1; i++)
            {
                KPK.Join(new Student("Ivan1", Telerik));
            }
        }

        [TestMethod]
        public void CourseJoinStudentInstance()
        {
            Course KPK = new Course("KPK");
            School Telerik = new School("Telerik");
            Student Ivan = new Student("Ivan Ivanov", Telerik);
            KPK.Join(Ivan);
            Assert.AreEqual(KPK.Students[0], Ivan, "The Student is not added correctly"); 
        }

        [TestMethod]
        public void CourseJoinStudent()
        {
            Course KPK = new Course("KPK");
            School Telerik = new School("Telerik");
            Student Ivan = new Student("Ivan Ivanov", Telerik);
            int currentStudentsCount = KPK.Students.Count;
            KPK.Join(Ivan);
            Assert.AreEqual(currentStudentsCount + 1, KPK.Students.Count, "Could not add student to the course");
        }

        [TestMethod]
        public void CourseLeaveExistingStudent()
        {
            Course KPK = new Course("KPK");
            School Telerik = new School("Telerik");
            Student Ivan = new Student("Ivan Ivanov", Telerik);
            Student Stoyan = new Student("Stoyan Hristov", Telerik);
            Student Maria = new Student("Maria Ivanova", Telerik);
            KPK.Join(Ivan);
            KPK.Join(Stoyan);
            KPK.Join(Maria);
            int currentStudentsCount = KPK.Students.Count;
            bool isRemoved = KPK.Leave(Stoyan);
            Assert.IsTrue(isRemoved, "Student is not existing to the course");
            Assert.AreEqual(currentStudentsCount - 1, KPK.Students.Count, "Student is not removed from students list");
        }

        [TestMethod]
        public void CourseLeaveNonExistingStudent()
        {
            Course KPK = new Course("KPK");
            School Telerik = new School("Telerik");
            Student Ivan = new Student("Ivan Ivanov", Telerik);
            Student Stoyan = new Student("Stoyan Hristov", Telerik);
            Student Maria = new Student("Maria Ivanova", Telerik);
            KPK.Join(Ivan);
            KPK.Join(Maria);
            int currentStudentsCount = KPK.Students.Count;
            bool isRemoved = KPK.Leave(Stoyan);
            Assert.IsFalse(isRemoved, "Non-existent student removed.");
            Assert.AreEqual(currentStudentsCount, KPK.Students.Count, "Non-existent student removed.");
        }

        [TestMethod]
        public void CourseToString()
        {
            Course KPK = new Course("KPK");
            School Telerik = new School("Telerik");
            Student Ivan = new Student("Ivan Ivanov", Telerik);
            Student Maria = new Student("Maria Ivanova", Telerik);
            KPK.Join(Ivan);
            KPK.Join(Maria);
            Assert.AreEqual(KPK.ToString(),
                string.Format(
                "Name = KPK; Students = {{ {0}, {1} }}.", Ivan, Maria)
                , "Course.ToString() is not correct.");
        }
    }
}
