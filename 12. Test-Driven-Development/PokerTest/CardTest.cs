﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Poker;

namespace PokerTest
{
    [TestClass]
    public class CardTest
    {
        [TestMethod]
        public void TestCardToString1()
        {
            Card card = new Card(CardFace.Seven, CardSuit.Diamonds);
            Assert.AreEqual("7♦", card.ToString(), "Card conversion to string is incorrect.");
        }

        [TestMethod]
        public void TestCardToString2()
        {
            Card card = new Card(CardFace.Ace, CardSuit.Hearts);
            Assert.AreEqual("A♥", card.ToString(), "Card conversion to string is incorrect.");
        }

        [TestMethod]
        public void TestCardToString3()
        {
            Card card = new Card(CardFace.Two, CardSuit.Clubs);
            Assert.AreEqual("2♣", card.ToString(), "Card conversion to string is incorrect.");
        }

        [TestMethod]
        public void TestCardToString4()
        {
            Card card = new Card(CardFace.Queen, CardSuit.Spades);
            Assert.AreEqual("Q♠", card.ToString(), "Card conversion to string is incorrect.");
        }

        [TestMethod]
        public void TestCardToString5()
        {
            Card card = new Card(CardFace.Ten, CardSuit.Spades);
            Assert.AreEqual("10♠", card.ToString(), "Card conversion to string is incorrect.");
        }
    }

}
