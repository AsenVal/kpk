﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Poker;

namespace PokerTest
{
    [TestClass]
    public class HandTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestHandConstructorNullCardsInput()
        {
            ICard[] cards = null;
            IHand hand = new Hand(cards);
        }

       /* [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestHandConstructorNullCardNameInput()
        {
            string cardNames = null;
            IHand hand = new Hand(cardNames);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestHandConstructorEmptyCardNameInput()
        {
            string cardNames = string.Empty;
            IHand hand = new Hand(cardNames);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestHandConstructorWhithspaceCardNameInpuut()
        {
            string cardNames = " ";
            IHand hand = new Hand(cardNames);
        }*/

        [TestMethod]
        public void TestHandToStringEmptyHand()
        {
            ICard[] cards = new ICard[0];
            IHand hand = new Hand(cards);

            Assert.AreEqual(string.Empty, hand.ToString(), "Hand constructor does not work correctly.");
        }

        [TestMethod]
        public void TestHandToString()
        {
            ICard[] cards = new ICard[5]
            {
                new Card(CardFace.Nine, CardSuit.Spades),
                new Card(CardFace.Two, CardSuit.Hearts),
                new Card(CardFace.Jack, CardSuit.Clubs),
                new Card(CardFace.Ace, CardSuit.Diamonds),
                new Card(CardFace.Five, CardSuit.Diamonds)
            };

            IHand hand = new Hand(cards);

            Assert.AreEqual("2♥ 5♦ 9♠ J♣ A♦", hand.ToString(), "Hand conversion to string does not work correctly.");
        }

        [TestMethod]
        public void TestHandToStringOneCard()
        {
            ICard[] cards = new ICard[1]
            {
                new Card(CardFace.Queen, CardSuit.Clubs)
            };

            IHand hand = new Hand(cards);

            Assert.AreEqual("Q♣", hand.ToString(), "Hand conversion to string does not work correctly.");
        }
    }
}
