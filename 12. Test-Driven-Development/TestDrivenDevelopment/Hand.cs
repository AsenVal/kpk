﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Poker
{
    public class Hand : IHand
    {
        public IList<ICard> Cards { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Hand"/> class.
        /// </summary>
        /// <param name="cards">The cards in the hand.</param>
        /// <exception cref="System.ArgumentNullException">Thrown when
        /// <paramref name="cards"/> is null.</exception>
        public Hand(IList<ICard> cards)
        {
            if (cards == null)
            {
                throw new ArgumentNullException("cards", "cards cannot be null.");
            }

            this.Cards = cards;
        }

        /// <summary>
        /// Sorts (the cards in) the hand in ascending order.
        /// </summary>
        public void Sort()
        {
            this.Cards = this.Cards.OrderBy(x => (int)x.Face).ThenBy(x => (int)x.Suit).ToList();
        }

        /// <summary>
        /// Returns the string representations of the cards in the hand,
        /// space-separated.
        /// </summary>
        /// <returns>A string containing the string representations
        /// of the cards in the hand.</returns>
        public override string ToString()
        {
            this.Sort();
            return string.Join<ICard>(" ", this.Cards);
        }
    }
}
