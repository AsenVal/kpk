﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Poker
{
    public class PokerHandsChecker : IPokerHandsChecker
    {
        public const int HandSize = 5;
        private Dictionary<CardFace, int> arrangedCards = new Dictionary<CardFace, int>();

        private KeyValuePair<CardFace, int>[] GetOrderedCardFaces(IHand hand)
        {
            foreach (CardFace face in Enum.GetValues(typeof(CardFace)))
            {
                this.arrangedCards[face] = 0;
            }

            foreach (ICard card in hand.Cards)
            {
                this.arrangedCards[card.Face]++;
            }

            return this.arrangedCards.OrderByDescending(x => x.Value).ToArray();
            //return this.arrangedCards.OrderByDescending(pair => pair.Value).ToArray();
        }


        public bool IsValidHand(IHand hand)
        {
            if (hand == null)
            {
                return false;
            }

            IList<ICard> cards = hand.Cards;

            if (cards.Count != HandSize)
            {
                return false;
            }

            //checks if cards are not equal
            for (int i = 0; i < cards.Count - 1; i++)
            {
                for (int j = i + 1; j < cards.Count; j++)
                {
                    if (cards[i].Equals(cards[j]))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public bool IsStraightFlush(IHand hand)
        {
            
            bool flush = this.IsStraight(hand);

            bool straight = this.IsFlush(hand);

            if (straight && flush)
            {
                return true;
            }
            return false;
        }

        public bool IsFourOfAKind(IHand hand)
        {
            var getOrderedCardFaces = this.GetOrderedCardFaces(hand);
            if (getOrderedCardFaces[0].Value == 4)
            {
                return true;
            }
            return false;
        }

        public bool IsFullHouse(IHand hand)
        {
            var getOrderedCardFaces = this.GetOrderedCardFaces(hand);
            if (getOrderedCardFaces[0].Value == 3 &&
                getOrderedCardFaces[1].Value == 2)
            {
                return true;
            }
            return false;
        }

        public bool IsFlush(IHand hand)
        {
            IList<ICard> cards = hand.Cards;

            for (int i = 1; i < cards.Count; i++)
            {
                if (cards[0].Suit != cards[i].Suit)
                {
                    return false;
                }
            }

            return true;
        }

        public bool IsStraight(IHand hand)
        {
            hand.Sort();

            if ((hand.Cards[4].Face - hand.Cards[0].Face) == 4)
            {
                return true;
            }

            if (hand.Cards[4].Face == CardFace.Ace &&
                hand.Cards[3].Face == CardFace.Five)
            {
                // a wheel: A, 2, 3, 4, 5
                return true;
            }

            return false;
        }

        public bool IsThreeOfAKind(IHand hand)
        {
            var getOrderedCardFaces = this.GetOrderedCardFaces(hand);
            if (getOrderedCardFaces[0].Value == 3 &&
                getOrderedCardFaces[1].Value == 1)
            {
                return true;
            }
            return false;
        }

        public bool IsTwoPair(IHand hand)
        {
            var getOrderedCardFaces = this.GetOrderedCardFaces(hand);
            if (getOrderedCardFaces[0].Value == 2 &&
                getOrderedCardFaces[1].Value == 2)
            {
                return true;
            }
            return false;
        }

        public bool IsOnePair(IHand hand)
        {
            var getOrderedCardFaces = this.GetOrderedCardFaces(hand);
            if (getOrderedCardFaces[0].Value == 2 &&
                getOrderedCardFaces[1].Value == 1 &&
                getOrderedCardFaces[2].Value == 1)
            {
                return true;
            }
            return false;
        }

        public bool IsHighCard(IHand hand)
        {
            throw new NotImplementedException();
        }

        public int CompareHands(IHand firstHand, IHand secondHand)
        {
            throw new NotImplementedException();
        }
    }
}
