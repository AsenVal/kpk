﻿using System;
using System.Linq;

namespace RectangleFigure
{
    public class Rectangle
    {
        private double width;
        private double height;

        public double Width
        {
            get { return this.width; }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("Width must be positive number");
                }
                else
                {
                    this.width = value;
                }
            }
        }

        public double Height
        {
            get { return this.height; }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("Width must be positive number");
                }
                else
                {
                    this.height = value;
                }
            }
        }        
       
        public Rectangle(double width, double height)
        {
            this.Width = width;
            this.Height = height;
        }

        /// <summary>
        /// Method to transform rectangle.
        /// </summary>
        /// <param name="rec">Rectangle object.</param>
        /// <param name="transformeAngle">Transformation angle.</param>
        /// <returns>Returns new rectangle object.</returns>
        public static Rectangle GetTransformedRectangle(Rectangle rec, double transformeAngle)
        {
            double width = Math.Abs(Math.Cos(transformeAngle)) * rec.Width + Math.Abs(Math.Sin(transformeAngle)) * rec.Height;
            double height = Math.Abs(Math.Sin(transformeAngle)) * rec.Width + Math.Abs(Math.Cos(transformeAngle)) * rec.Height;
            Rectangle result = new Rectangle(width, height);
            return result;
        }
    }
}